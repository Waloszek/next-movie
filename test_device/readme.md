test_device
===

Any `flutter_test` tests that have to be run on an actual device should be placed here.

To run the tests use `flutter run test_device/tests.dart`.
