import 'dart:io' show exit;

const _testTimeout = 6;

void main() {
  // Run your tests here
  Future.delayed(const Duration(seconds: _testTimeout), () => exit(0));
}
