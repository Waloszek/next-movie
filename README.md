# Flutter App Templates

A collection of different flutter app templates.

## 1. Creating a new project from template
This tutorial will focus on Android Studio (on Mac).
<br>
1. From AndroidStudio choose `File > New > Project from Version Control > Git`.
2. Put in the URL to the repository containing the template.
3. Click `Test` to check the connection.
4. Specify the directory path to your project (choose your own project name): 
`/Users/user_name/project_directory/project_name`. 

## - Using `rename.sh`

Thanks to `rename.sh` you can change the default values of 3 strings 
- The App's Bundle ID (`com.waloszek.nextmovie`)
- Project Name (`next_movie`)
- Displayed App's Name (`AppTemplate`)

Preferably you should do this as the first thing after creating a project.
<br>
Be careful when choosing which values you change to. 
<br>
<b>MUST BE UNIQUE ONES!!</b>
<br><br>
The project name should be preferably named just like the project's folder.

You can use the tool multiple times if you didn't change any strings by yourself. 
<br> 
Otherwise change the `currentValues` in the script's code.

When modifying the script be careful to always exclude your `.git` repository folder from being edited unwillingly.

Execute with `/.rename.sh` in the terminal.

## - Using `test.sh`

You can run all your tests at once by putting them inside this script. 
<br><br>
Execute with `/.test.sh` in the terminal.

## Changing notification icon in Android
Change `ic_notification.png` for each mipmap folder and add the same file as `ic_launcher.png` in `res/drawable`.
Do this for each android flavour separately or just for `android/app/main/src/main`.

## Deploying app using CI
# Android:
1. Add `key.jks` to `android/` 
(https://docs.fastlane.tools/actions/supply/)
2. Add required passwords in 
    `android/key.properties`
3. Uncomment the following line in `android/app/build.gradle`
    ```
    storeFile file(keystoreProperties['storeFile'])
    ``` 
4. Specify app's production ID in `android/app/build.gradle`
    ```
    prod {
        applicationId "com.yourappid"
        dimension "default"
    }
    ```
# iOS:
1. Add required ID's in 
    `ios/fastlane/Appfile`
2. Add password to 
    `ios/fastlane/Fastfile`
3. Add password, git url and username to 
    `ios/fastlane/Matchfile`