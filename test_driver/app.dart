import 'package:flutter_driver/driver_extension.dart';
import 'package:next_movie/main_dev.dart' as app;

void main() {
  enableFlutterDriverExtension();
  app.main();
}