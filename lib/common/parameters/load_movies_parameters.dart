import 'package:equatable/equatable.dart';
import 'package:next_movie/common/parameters/list_movie.dart';
import 'package:next_movie/domain/entities/movie_list_type.dart';
import 'package:meta/meta.dart';

class LoadMoviesParameters extends Equatable {
  final MovieListType listType;
  final int page;
  final Sink<ListMovie> listMovieSink;

  const LoadMoviesParameters({
    @required this.listType,
    @required this.page,
    @required this.listMovieSink,
  });

  @override
  List<Object> get props => [
        listType,
        page,
        listMovieSink,
      ];
}
