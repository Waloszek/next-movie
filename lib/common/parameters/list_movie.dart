import 'package:equatable/equatable.dart';

import 'package:next_movie/domain/entities/movie.dart';
import 'package:next_movie/domain/entities/movie_list_type.dart';
import 'package:meta/meta.dart';

class ListMovie extends Equatable {
  final Movie movie;
  final MovieListType listType;

  const ListMovie({
    @required this.movie,
    @required this.listType,
  })  : assert(movie != null),
        assert(listType != null);

  @override
  List<Object> get props => [
        movie,
        listType,
      ];
}
