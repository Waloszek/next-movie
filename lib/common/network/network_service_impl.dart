import 'package:dio/dio.dart';
import 'package:next_movie/common/network/network_service.dart';
import 'package:next_movie/common/network/request.dart';
import 'package:next_movie/common/result/failure_result.dart';
import 'package:next_movie/common/result/result.dart';
import 'package:next_movie/common/result/success_result.dart';
import 'package:next_movie/common/network/request_exceptions.dart';
import 'package:meta/meta.dart';

import '../blocs/network/network_bloc.dart';
import '../blocs/network/network_event.dart';
import '../error/failure.dart';
import 'network_info.dart';

class NetworkServiceImpl extends NetworkService {
  final NetworkInfo networkInfo;
  final NetworkBloc networkBloc;

  NetworkServiceImpl({
    @required this.networkInfo,
    @required Dio dio,
    @required this.networkBloc,
  })  : assert(networkInfo != null),
        assert(networkBloc != null),
        super(dio: dio);

  @override
  Future<Result<T, Failure>> make<T>(Request<T> request) async {
    Future<dynamic> requestFunction() => super.make(request);
    return _perform(requestFunction, request);
  }

  @override
  Future<Result<T, Failure>> upload<T>(UploadRequest<T> request) async {
    Future<dynamic> requestFunction() => super.upload(request);
    return _perform(requestFunction, request);
  }

  Future<Result<T, Failure>> _perform<T>(Function requestFunction, Request<T> request) async {
    if (!await networkInfo.isConnected) {
      _notifyBlocNetworkIsUnavailable();
      return FailureResult<T, NetworkFailure>(NetworkFailure());
    }
    try {
      final result = await requestFunction.call();
      _notifyBlocNetworkIsAvailable();
      return SuccessResult(result as T);
    } on ConnectionException {
      _notifyBlocNetworkIsUnavailable();
      return FailureResult<T, NetworkFailure>(NetworkFailure());
    } on UnauthorizedException {
      return FailureResult<T, AuthenticationFailure>(AuthenticationFailure());
    } on Exception {
      _notifyBlocOfUnexpectedError();
      return FailureResult<T, UnexpectedFailure>(UnexpectedFailure());
    }
  }

  void _notifyBlocNetworkIsAvailable() {
    networkBloc.add(NetworkIsAvailableEvent());
  }

  void _notifyBlocNetworkIsUnavailable() {
    networkBloc.add(NetworkIsUnavailableEvent());
  }

  void _notifyBlocOfUnexpectedError() {
    networkBloc.add(UnexpectedNetworkErrorEvent());
  }
}
