import 'dart:async';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:next_movie/common/blocs/network/network_bloc.dart';
import 'package:next_movie/common/environments/environment.dart';
import 'package:next_movie/common/environments/environment_dev.dart';
import 'package:next_movie/common/environments/environment_prod.dart';
import 'package:next_movie/common/network/network_service.dart';
import 'package:next_movie/common/network/network_service_impl.dart';
import 'package:next_movie/common/network/web_network_info.dart';
import 'package:next_movie/common/parameters/list_movie.dart';
import 'package:next_movie/data/data_sources/local/local_settings_data_source.dart';
import 'package:next_movie/data/data_sources/movie_data_source.dart';
import 'package:next_movie/data/data_sources/remote/remote_movie_data_source.dart';
import 'package:next_movie/data/data_sources/settings_data_source.dart';
import 'package:next_movie/data/repositories/movie_repository_impl.dart';
import 'package:next_movie/data/repositories/settings_repository_impl.dart';
import 'package:next_movie/domain/repositories/movie_repository.dart';
import 'package:next_movie/domain/repositories/settings_repository.dart';
import 'package:next_movie/domain/use_cases/load_movies.dart';
import 'package:next_movie/domain/use_cases/settings/load_settings.dart';
import 'package:next_movie/domain/use_cases/settings/save_settings.dart';
import 'package:next_movie/presentation/features/movie_list/bloc/movie_list_page_bloc.dart';
import 'package:next_movie/presentation/features/settings/bloc/settings_bloc.dart';
import 'package:kiwi/kiwi.dart';

import '../network/network_info.dart';

class Injector {
  static Container container;
  static final T Function<T>([String name]) resolve = container.resolve;

  static void setup() {
    container = Container();

    _common();

    _switchBasedOnEnvironment(
      devSetup: _commonDevelopment,
      prodSetup: _commonProduction,
    );

    if (kIsWeb) {
      _webCommon();
      _switchBasedOnEnvironment(
        devSetup: _webDevelopment,
        prodSetup: _webProduction,
      );
    } else {
      _mobileCommon();
      _switchBasedOnEnvironment(
        devSetup: _mobileDevelopment,
        prodSetup: _mobileProduction,
      );
    }
  }

  static void _switchBasedOnEnvironment({
    @required void Function() devSetup,
    @required void Function() prodSetup,
  }) {
    switch (Environment.current.runtimeType) {
      case DevelopmentEnvironment:
        devSetup?.call();
        break;
      case ProductionEnvironment:
        prodSetup?.call();
        break;
    }
  }

  static void _common() {
    //                 //
    //  N E T W O R K  //
    //                 //
    container.registerSingleton(
      (c) => Dio(BaseOptions())..interceptors.addAll(c.resolve()),
    );

    container.registerSingleton<NetworkService, NetworkServiceImpl>(
      (c) => NetworkServiceImpl(
        networkInfo: c.resolve(),
        dio: c.resolve(),
        networkBloc: c.resolve(),
      ),
    );
    container.registerInstance<Iterable<Interceptor>, List<Interceptor>>(<Interceptor>[]);

    //                         //
    //  D A T A   S O U R C E  //
    //                         //
    container.registerSingleton<SettingsDataSource, LocalSettingsDataSource>(
      (c) => LocalSettingsDataSource(),
    );
    container.registerSingleton<MovieDataSource, RemoteMovieDataSource>(
      (container) => RemoteMovieDataSource(networkService: container.resolve()),
    );

    //                       //
    //  R E P O S I T O R Y  //
    //                       //
    container.registerSingleton<SettingsRepository, SettingsRepositoryImpl>(
      (c) => SettingsRepositoryImpl(settingsDataSource: c.resolve()),
    );
    container.registerSingleton<MovieRepository, MovieRepositoryImpl>(
      (container) => MovieRepositoryImpl(movieDataSource: container.resolve()),
    );

    //                   //
    //  U S E   C A S E  //
    //                   //
    container.registerSingleton((c) => LoadSettings(settingsRepository: c.resolve()));
    container.registerSingleton((c) => SaveSettings(settingsRepository: c.resolve()));
    container.registerSingleton((c) => LoadMovies(movieRepository: c.resolve()));

    //             //
    //  B L O C S  //
    //             //
    container.registerSingleton(
      (c) => SettingsBloc(
        loadSettings: c.resolve(),
        saveSettings: c.resolve(),
      ),
    );
    container.registerSingleton((c) => NetworkBloc(networkInfo: c.resolve()));

    //                     //
    //  P A G E   B L O C  //
    //                     //
    container.registerSingleton(
      (c) => MovieListPageBloc(
        loadMovies: c.resolve(),
        listMovieStreamController: c.resolve('listMovieStreamController'),
      ),
    );
    //         //
    //  OTHER  //
    //         //
    container.registerInstance(StreamController<ListMovie>(), name: 'listMovieStreamController');
  }

  static void _commonDevelopment() {}

  static void _commonProduction() {}

  //     //
  // WEB //
  //     //
  static void _webCommon() {
    container.registerSingleton<NetworkInfo, WebNetworkInfoImpl>(
      (c) => WebNetworkInfoImpl(),
    );
  }

  static void _webDevelopment() {}
  static void _webProduction() {}

  //        //
  // MOBILE //
  //        //
  static void _mobileCommon() {
    container.registerSingleton<NetworkInfo, NetworkInfoImpl>(
      (c) => NetworkInfoImpl(c.resolve()),
    );
    container.registerSingleton((c) => DataConnectionChecker());
  }

  static void _mobileDevelopment() {}
  static void _mobileProduction() {}
}
