import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:next_movie/common/blocs/network/network_event.dart';
import 'package:next_movie/common/blocs/network/network_state.dart';
import 'package:next_movie/common/network/network_info.dart';

import 'package:meta/meta.dart';

class NetworkBloc extends Bloc<NetworkEvent, NetworkState> {
  final NetworkInfo networkInfo;

  NetworkBloc({@required this.networkInfo}) : assert(networkInfo != null) {
    _setUpNetworkListener();
  }

  void _setUpNetworkListener() {
    networkInfo.onStatusChange.listen(
      (status) {
        // We ignore not connected state here to prevent
        // NetworkDependent from showing an error page unnecessarily
        if (status == DataConnectionStatus.connected) {
          add(NetworkIsAvailableEvent());
        }
      },
    );
  }

  @override
  NetworkState get initialState => NetworkAvailableState();

  @override
  Stream<NetworkState> mapEventToState(
    NetworkEvent event,
  ) async* {
    if (event is NetworkIsAvailableEvent) {
      yield NetworkAvailableState();
    } else if (event is NetworkIsUnavailableEvent) {
      yield NetworkUnavailableState();
    } else if (event is UnexpectedNetworkErrorEvent) {
      yield UnexpectedNetworkErrorState();
    }
  }
}
