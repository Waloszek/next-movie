import 'package:equatable/equatable.dart';

abstract class NetworkState extends Equatable {
  const NetworkState();

  @override
  List<Object> get props => [];
}

class NetworkAvailableState extends NetworkState {}

class NetworkUnavailableState extends NetworkState {}

class UnexpectedNetworkErrorState extends NetworkState {}
