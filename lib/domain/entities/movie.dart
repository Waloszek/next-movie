import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:next_movie/domain/entities/movie_rating.dart';

abstract class Movie extends Equatable {
  final int id;
  final String imdbId;
  final String title;
  final String poster;
  final List<MovieRating> ratings;

  const Movie({
    @required this.id,
    this.imdbId,
    this.title,
    this.poster,
    this.ratings,
  }) : assert(id != null);

  @override
  List<Object> get props => [
        imdbId,
        title,
        poster,
        ratings,
      ];
}
