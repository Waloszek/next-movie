import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:next_movie/common/error/page_parameters_invalid_error.dart';

abstract class PageBody<P extends PageBodyParameters> extends StatefulWidget {
  final P parameters;

  const PageBody(this.parameters);

  State createMobileState();

  State createWebState();

  @override
  State<StatefulWidget> createState() {
    if (kIsWeb) {
      return createWebState();
    } else {
      return createMobileState();
    }
  }
}

abstract class PageBodyParameters {
  final BuildContext context;

  PageBodyParameters({@required this.context}) : assert(context != null);

  bool validate();
}

abstract class PageParametersBuilder<P extends PageBodyParameters> {
  @protected
  P parameters;

  PageParametersBuilder(this.parameters);

  P build() {
    final parametersValid = parameters.validate();
    if (parametersValid) {
      return parameters;
    } else {
      throw PageParametersInvalidError();
    }
  }
}
