import 'package:flutter/material.dart';

import '../common/utils/bloc_wrapper.dart';
import 'features/home/pages/home_page.dart';
import 'pages/splash_page.dart';

final Map<String, WidgetBuilder> navigationRoutes = <String, WidgetBuilder>{
  SplashPage.routeName: (BuildContext context) => AppPages.splash,
  HomePage.routeName: (BuildContext context) => AppPages.home,
};

abstract class AppPages {
  static Widget get splash => const SplashPage();

  static Widget get home => BlocWrapper(HomePage()).build();
}
