import 'package:intl/intl.dart';

mixin AppLocalizationMessages {
  String exit() => Intl.message(
        'Exit',
        name: 'exit',
        desc: 'The word used to exit the current window',
      );

  String cancel() => Intl.message(
        'Cancel',
        name: 'cancel',
        desc: 'The word used to cancel an action',
      );

  String ok() => Intl.message(
        'OK',
        name: 'ok',
        desc: 'OK',
      );

  String unexpectedError() => Intl.message(
        'An unexpected error occured. Please try again later.',
        name: 'unexpectedError',
        desc: 'Error message when an unexpected error occurs',
      );

  String awaitingConnection() => Intl.message(
        'Awaiting connection...',
        name: 'awaitingConnection',
        desc: 'Text in view showing network unavailability',
      );

  String unexpectedErrorPressToRetry() => Intl.message(
        'An unexpected error occured\Tap to try again',
        name: 'unexpectedErrorPressToRetry',
        desc: 'Text in view showing network error',
      );

  String noInternetFound() => Intl.message(
        'No internet connection found',
        name: 'noInternetFound',
        desc: 'Error message when user has no internet available',
      );

  String authenticationFailed() => Intl.message(
        'Authentication failed',
        name: 'authenticationFailed',
      );

  String movies() => Intl.message(
        'Movies',
        name: 'movies',
      );

  String tv() => Intl.message(
        'TV',
        name: 'tv',
      );

  String settings() => Intl.message(
        'Settings',
        name: 'settings',
      );

  String nowPlaying() => Intl.message(
        'Now Playing',
        name: 'nowPlaying',
      );

  String popular() => Intl.message(
        'Popular',
        name: 'popular',
      );

  String topRated() => Intl.message(
        'Top rated',
        name: 'topRated',
      );

  String upcoming() => Intl.message(
        'Upcoming',
        name: 'upcoming',
      );

  String noPosterAvailable() => Intl.message(
        'No poster available',
        name: 'noPosterAvailable',
      );
}
