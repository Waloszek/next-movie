import 'package:flutter/material.dart';
import 'package:next_movie/common/blocs/network/network_bloc.dart';
import 'package:next_movie/common/blocs/network/network_state.dart';
import 'package:next_movie/presentation/localization/app_localizations.dart';
import 'package:next_movie/presentation/widgets/network_dependent/network_error_message.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/// This widget handles network dependent errors with a dedicated error view.
class NetworkDependent extends StatelessWidget {
  /// Widget to be drawn when no network errors occur.
  final Widget child;

  /// Function to be executed when network becomes available again
  /// or user wishes to retry the request upon an unexpected error.
  final void Function() onNetworkRetry;

  const NetworkDependent({
    Key key,
    @required this.child,
    this.onNetworkRetry,
  })  : assert(child != null),
        super(key: key);

  @override
  Widget build(BuildContext context) => _buildBlocListener();

  BlocListener<NetworkBloc, NetworkState> _buildBlocListener() {
    return BlocListener<NetworkBloc, NetworkState>(
      listener: (context, state) {
        if (state is NetworkAvailableState) {
          onNetworkRetry?.call();
        }
      },
      child: _buildBlocBuilder(),
    );
  }

  Widget _buildBlocBuilder() {
    return BlocBuilder<NetworkBloc, NetworkState>(
      builder: (context, state) {
        if (state is NetworkAvailableState) {
          return child;
        } else if (state is NetworkUnavailableState) {
          return _buildNetworkUnavailableMessage(context);
        } else if (state is UnexpectedNetworkErrorState) {
          return _buildUnexpectedErrorMessage(context);
        }
        throw UnsupportedError('$state is not supported');
      },
    );
  }

  Widget _buildNetworkUnavailableMessage(BuildContext context) {
    return NetworkErrorMessage(
      title: AppLocalizations.of(context).awaitingConnection(),
    );
  }

  Widget _buildUnexpectedErrorMessage(BuildContext context) {
    return InkWell(
      onTap: onNetworkRetry,
      child: NetworkErrorMessage(
        title: AppLocalizations.of(context).unexpectedErrorPressToRetry(),
      ),
    );
  }
}
