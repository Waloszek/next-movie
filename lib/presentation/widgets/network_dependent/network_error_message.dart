import 'package:flutter/material.dart';
import 'package:next_movie/presentation/theme/app_text_styles.dart';
import 'package:next_movie/presentation/theme/color/app_colors.dart';

class NetworkErrorMessage extends StatelessWidget {
  final AssetImage image;
  final String title;

  const NetworkErrorMessage({
    Key key,
    @required this.title,
    this.image,
  })  : assert(title != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if (image != null) _buildImage(context),
        _buildTitleText(context),
      ],
    );
  }

  Widget _buildImage(BuildContext context) {
    return Image(
      height: 113,
      image: image,
    );
  }

  Widget _buildTitleText(BuildContext context) {
    return Container(
      width: double.maxFinite,
      margin: const EdgeInsets.only(top: 20),
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: AppTextStyles.body1(
          context,
          color: AppColors.secondaryContent(context),
        ),
      ),
    );
  }
}
