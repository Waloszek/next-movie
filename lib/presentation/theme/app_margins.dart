/// Common margin values used throughout the app.
abstract class AppMargins {
  static const double horizontal = 20;
}
