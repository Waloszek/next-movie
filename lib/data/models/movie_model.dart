import 'package:next_movie/data/models/movie_rating_model.dart';
import 'package:next_movie/domain/entities/movie.dart';
import 'package:next_movie/domain/entities/movie_rating.dart';
import 'package:meta/meta.dart';

// ignore_for_file: argument_type_not_assignable
class MovieModel extends Movie {
  const MovieModel({
    @required int id,
    String imdbId,
    String title,
    String poster,
    List<MovieRating> ratings,
  }) : super(
          id: id,
          imdbId: imdbId,
          title: title,
          poster: poster,
          ratings: ratings,
        );

  MovieModel copyWith({List<MovieRating> ratings}) {
    return MovieModel(
      id: id,
      imdbId: imdbId,
      title: title,
      poster: poster,
      ratings: ratings ?? this.ratings,
    );
  }

  factory MovieModel.fromJson(Map<String, dynamic> json) {
    final voteAverage = json["vote_average"];
    final hasVotes = voteAverage != 0.0;
    return MovieModel(
      id: json['id'],
      imdbId: json['imdb_id'],
      title: json["title"],
      poster: json["poster_path"] != null ? 'http://image.tmdb.org/t/p/w342${json["poster_path"]}' : null,
      ratings: hasVotes ? [MovieRatingModel.tmdb(voteAverage)] : [],
    );
  }
}
