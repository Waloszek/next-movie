import 'package:next_movie/common/error/failure.dart';
import 'package:next_movie/common/parameters/load_movies_parameters.dart';
import 'package:next_movie/common/result/result.dart';
import 'package:next_movie/data/data_sources/movie_data_source.dart';
import 'package:next_movie/domain/repositories/movie_repository.dart';
import 'package:meta/meta.dart';

class MovieRepositoryImpl extends MovieRepository {
  final MovieDataSource movieDataSource;

  MovieRepositoryImpl({
    @required this.movieDataSource,
  }) : assert(movieDataSource != null);

  @override
  Future<Result<void, Failure>> getMovies(LoadMoviesParameters parameters) {
    return movieDataSource.getMovies(parameters);
  }
}
