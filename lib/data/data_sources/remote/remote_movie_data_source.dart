import 'dart:async';

import 'package:next_movie/common/error/failure.dart';
import 'package:next_movie/common/network/network_service.dart';
import 'package:next_movie/common/parameters/list_movie.dart';
import 'package:next_movie/common/parameters/load_movies_parameters.dart';
import 'package:next_movie/common/result/failure_result.dart';
import 'package:next_movie/common/result/result.dart';
import 'package:next_movie/common/result/success_result.dart';
import 'package:next_movie/data/data_sources/movie_data_source.dart';
import 'package:next_movie/data/models/movie_model.dart';
import 'package:next_movie/data/network/get_imdb_ratings_request.dart';
import 'package:next_movie/data/network/get_movie_details_request.dart';
import 'package:next_movie/data/network/get_now_playing_movies_request.dart';
import 'package:next_movie/data/network/get_popular_movies_request.dart';
import 'package:next_movie/data/network/get_tmdb_movie_list_request.dart';
import 'package:next_movie/data/network/get_top_rated_movies_request.dart';
import 'package:next_movie/data/network/get_upcoming_movies_request.dart';
import 'package:meta/meta.dart';
import 'package:next_movie/domain/entities/movie.dart';
import 'package:next_movie/domain/entities/movie_list_type.dart';
import 'package:next_movie/domain/entities/movie_rating.dart';

// ignore_for_file: return_of_invalid_type
class RemoteMovieDataSource extends MovieDataSource {
  final NetworkService networkService;

  RemoteMovieDataSource({@required this.networkService}) : assert(networkService != null);

  @override
  Future<Result<void, Failure>> getMovies(LoadMoviesParameters parameters) async {
    final request = getRequestForListType(parameters.listType, parameters.page);
    final movieIdsResult = await networkService.make(request) as Result<List<int>, Failure>;
    return movieIdsResult.fold(
      onSuccess: (movieIds) async {
        await _getMovieDetails(movieIds, parameters.listMovieSink, parameters.listType);
        return SuccessResult(null);
      },
      onFailure: (failure) => FailureResult(failure),
    );
  }

  GetTmdbMovieListRequest getRequestForListType(MovieListType listType, int page) {
    switch (listType) {
      case MovieListType.nowPlaying:
        return GetNowPlayingMoviesRequest(page: page);
      case MovieListType.upcoming:
        return GetUpcomingMoviesRequest(page: page);
      case MovieListType.popular:
        return GetPopularMoviesRequest(page: page);
      case MovieListType.topRated:
        return GetTopRatedMoviesRequest(page: page);
      default:
        throw UnimplementedError('$listType is not supported');
    }
  }

  Future<void> _getMovieDetails(
    List<int> movieIds,
    Sink<ListMovie> listMovieSink,
    MovieListType movielistType,
  ) async {
    return Future.forEach(movieIds, (int id) async {
      final movieResult = await _getMovie(id);
      await movieResult.fold(
        onSuccess: (movie) => _handleMovieReviews(movie, listMovieSink, movielistType),
        onFailure: (_) {},
      );
    });
  }

  Future<Result<Movie, Failure>> _getMovie(int movieId) async {
    return networkService.make(GetMovieDetailsRequest(id: movieId));
  }

  Future<void> _handleMovieReviews(
    Movie movie,
    Sink<ListMovie> listMovieSink,
    MovieListType movielistType,
  ) async {
    if (movie.imdbId != null) {
      await _getMovieReviews(movie, listMovieSink, movielistType);
    } else {
      // Getting additional reviews is not possible so finish by returning movie.
      listMovieSink.add(ListMovie(movie: movie, listType: movielistType));
    }
  }

  Future<void> _getMovieReviews(
    Movie movie,
    Sink<ListMovie> listMovieSink,
    MovieListType movielistType,
  ) async {
    final ratingsResult = await _getMovieRatings(movie.imdbId);
    ratingsResult.fold(
      onSuccess: (additionalRatings) {
        final movieWithNewRatings =
            (movie as MovieModel).copyWith(ratings: List.from(movie.ratings)..addAll(additionalRatings));
        final listMovie = ListMovie(movie: movieWithNewRatings, listType: movielistType);
        return listMovieSink.add(listMovie);
      },
      onFailure: (_) => listMovieSink.add(ListMovie(movie: movie, listType: movielistType)),
    );
  }

  Future<Result<List<MovieRating>, Failure>> _getMovieRatings(String imdbId) async {
    final ratingsResult = await networkService.make(
      GetImdbRatingsRequest(imdbId: imdbId),
    ) as Result<List<MovieRating>, Failure>;
    return ratingsResult.fold(
      onSuccess: (newRatings) => SuccessResult(newRatings),
      onFailure: (failure) => FailureResult(failure),
    );
  }
}
