import 'package:next_movie/common/error/failure.dart';
import 'package:next_movie/common/parameters/load_movies_parameters.dart';
import 'package:next_movie/common/result/result.dart';

abstract class MovieDataSource {
  Future<Result<void, Failure>> getMovies(LoadMoviesParameters parameters);
}
