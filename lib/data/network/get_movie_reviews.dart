import 'package:next_movie/common/environments/environment.dart';
import 'package:next_movie/common/network/request.dart';
import 'package:next_movie/data/models/movie_rating_model.dart';
import 'package:next_movie/domain/entities/movie_rating.dart';
import 'package:meta/meta.dart';

class GetMovieRatingsRequest extends Request<List<MovieRating>> {
  GetMovieRatingsRequest({
    @required String imdbId,
  })  : assert(imdbId != null),
        super(
          path: 'http://www.omdbapi.com/?apikey=${Environment.current.omdbApiKey}&i=$imdbId&plot=full',
        );

  @override
  List<MovieRating> createResponse(dynamic json) {
    // FIXME: Ratings returned from omdb need to be cleaned and casted to numers
    final ratings = <MovieRating>[];

    const scoreNotAvailable = 'N/A';
    final metascore = json['Metascore'] as int;
    final imdbRating = json['imdbRating'] as double;

    if (metascore != scoreNotAvailable) {
      ratings.add(MovieRatingModel.metacritic(metascore));
    }
    if (imdbRating != scoreNotAvailable) {
      ratings.add(MovieRatingModel.imdb(imdbRating));
    }

    json['Ratings'].forEach(
      (ratingJson) {
        if (ratingJson['Source'] == "Rotten Tomatoes") {
          ratings.add(MovieRatingModel.rottenCritics(ratingJson['Value'] as double));
        }
      },
    );

    return ratings;
  }
}
